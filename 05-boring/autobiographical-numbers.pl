#!/usr/bin/env perl
use 5.024;
use warnings;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use JSON::PP 'encode_json';
use Storable 'dclone';
use List::Util 'min';

$|++;
printout(solution => autobiographical_numbers(shift // 4));

sub autobiographical_numbers ($n) {
    my @solution;
    if ($n == 4) {
        @solution = (1, 2, 1, 0); # also good: (2, 0, 2, 0)
    }
    elsif ($n > 6) {
        @solution = (0) x $n;
        @solution[0, 1, 2, $n - 4] = ($n - 4, 2, 1, 1);
    }
    return {solution => [map {+{$_ => 1}} @solution]};
}

sub printout ($phase, $status, $exception = undef) {
   if ($phase eq 'backtrack') {
      if ($@) {
         (my $e = $@) =~ s{\sat\s.*?\sline\s[0-9]+\.\s+\z}{}mxs;
         $phase = "backtrack[$e]";
      }
      else {
         $phase = 'explore';
      }
   } ## end if ($phase eq 'backtrack')
   say $phase, ' => ', encode_json [
      map {
         my @candidates = sort { $a <=> $b } keys $_->%*;
             @candidates > 1 ? \@candidates
           : @candidates > 0 ? 0 + $candidates[0]
           : '[]'
      } $status->{solution}->@*
   ];
} ## end sub printout
