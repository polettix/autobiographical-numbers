# Autobiographical Numbers

This repository contains an example solver for finding [Autobiographical
numbers][] and is paired with the series of posts in blog [ETOOBUSY][].

Different *stages* of the implementation are contained in the several
sub-directories, ordered by their increasing inclusion of more code.

If you want to run the code in one stage, you can take advantage of the
provided script `run.sh`, like this:

```shell
$ ./run.sh 01-basic
solution => [1,2,1,0]
```

You can also set the `VERBOSE` environment variable to follow all progress
in the search:

```shell
$ export VERBOSE=1
$ ./run.sh 01-basic
validating => [["0","1","2","3"],["0","1","2","3"],["0","1","2","3"],["0","1","2","3"]]
pruned => [["0","1","2","3"],["0","1","2","3"],["0","1","2","3"],["0","1","2","3"]]
...
validating => [1,2,0,0]
backtrack[invalid amount if 0] => [1,2,0,0]
validating => [1,2,1,0]
pruned => [1,2,1,0]
solution => [1,2,1,0]
```

The contents of this repository are licensed according to the Apache
License 2.0 (see file `LICENSE` in the project's root directory):

>  Copyright 2020 by Flavio Poletti
>
>  Licensed under the Apache License, Version 2.0 (the "License");
>  you may not use this file except in compliance with the License.
>  You may obtain a copy of the License at
>
>      http://www.apache.org/licenses/LICENSE-2.0
>
>  Unless required by applicable law or agreed to in writing, software
>  distributed under the License is distributed on an "AS IS" BASIS,
>  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>  See the License for the specific language governing permissions and
>  limitations under the License.

[ETOOBUSY]: https://github.polettix.it/ETOOBUSY/
[Autobiographical numbers]: https://github.polettix.it/ETOOBUSY/2020/04/08/autobiographical-numbers/
