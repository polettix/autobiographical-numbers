#!/usr/bin/env perl
use 5.024;
use warnings;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use JSON::PP 'encode_json';
use Storable 'dclone';
use List::Util 'min';

$|++;
printout(solution => autobiographical_numbers(shift // 4));

# Generic solver for constraint programming
sub solve_by_constraints {
   my %args = (@_ && ref($_[0])) ? %{$_[0]} : @_;
   my @reqs = qw< constraints is_done search_factory start >;
   exists($args{$_}) || die "missing parameter '$_'" for @reqs;
   my ($constraints, $done, $factory, $state, @stack) = @args{@reqs};
   my $logger = $args{logger} // undef;
   while ('necessary') {
      last if eval {    # eval - constraints might complain loudly...
         $logger->(validating => $state) if $logger;
         my $changed = -1;
         while ($changed != 0) {
            $changed = 0;
            $changed += $_->($state) for @$constraints;
            $logger->(pruned => $state) if $logger;
         }
         $done->($state) || (push(@stack, $factory->($state)) && undef);
      };
      $logger->("backtrack" => $state, $@) if $logger;
      while (@stack) {
         last if $stack[-1]->($state);
         pop @stack;
      }
      return unless @stack;
   } ## end while ('necessary')
   return $state;
} ## end sub solve_by_constraints

# an exercise in constraint programming
sub autobiographical_numbers ($n) {
   my $solution = [
      map {
         +{map { $_ => 1 } 0 .. $n - 2}    # "n-1" is always 0
      } 1 .. $n - 1
   ];
   push $solution->@*, {0 => 1};           # "n-1" is always 0
   my @constraints =
     map { main->can('constraint_' . $_) } qw< basic weighted_sum >;
   my $state = solve_by_constraints(
      constraints    => \@constraints,
      is_done        => \&is_done,
      search_factory => \&explore,
      start          => {solution => $solution},
      logger         => ($ENV{VERBOSE} ? \&printout : undef),
   );
} ## end sub autobiographical_numbers ($n)

sub printout ($phase, $status, $exception = undef) {
   if ($phase eq 'backtrack') {
      if ($@) {
         (my $e = $@) =~ s{\sat\s.*?\sline\s[0-9]+\.\s+\z}{}mxs;
         $phase = "backtrack[$e]";
      }
      else {
         $phase = 'explore';
      }
   } ## end if ($phase eq 'backtrack')
   say $phase, ' => ', encode_json [
      map {
         my @candidates = sort { $a <=> $b } keys $_->%*;
             @candidates > 1 ? \@candidates
           : @candidates > 0 ? 0 + $candidates[0]
           : '[]'
      } $status->{solution}->@*
   ];
} ## end sub printout

sub is_done ($status) {
   return scalar(grep { keys $_->%* > 1 } $status->{solution}->@*) == 0;
}

sub explore ($status) {
   my $solution = dclone($status->{solution});    # our working solution
   my $n        = $solution->@*;

   # this investigates a single slot...
   my $slot_id = 0;
   while ($slot_id < $n) {
      last if keys $solution->[$slot_id]->%* > 1;
      $slot_id++;
   }
   die 'wtf?!?' if $slot_id >= $n;
   my @candidates = sort { $a <=> $b } keys $solution->[$slot_id]->%*;
   my $amount;
   return sub ($status) {                         # get "next" solution
      return unless @candidates;
      $amount = shift @candidates;
      $status->{solution} = dclone($solution);
      $status->{solution}[$slot_id] = {$amount => 1};
      return 1;
   };
} ## end sub explore ($status)

sub constraint_basic ($status) {    # little pruning, mostly checking
   my $solution = $status->{solution};
   my %count_for;
   my %is_exact = map { $_ => 1 } 0 .. $#$solution;
   for my $i (0 .. $#$solution) {
      my $slot       = $solution->[$i];
      my @candidates = keys $slot->%*;
      die "no candidate for $i" unless @candidates;
      my $exact = @candidates == 1;
      for my $candidate (@candidates) {
         $count_for{$candidate}++;
         $is_exact{$candidate} = 0 unless $exact;
      }
   } ## end for my $i (0 .. $#$solution)
   my $changes = 0;
   for my $i (0 .. $#$solution) {
      my $amount = $count_for{$i} // 0;
      my $slot   = $solution->[$i];
      if ($is_exact{$i}) {
         die "invalid amount if $i" unless exists $slot->{$amount};
         if (scalar keys $slot->%* > 1) {    # prune
            $slot->%* = ($amount => 1);
            $changes++;
         }
      } ## end if ($is_exact{$i})
      else {
         for my $needed (keys $slot->%*) {
            if ($needed > $amount) {
               delete $slot->{$needed};      # this can't be fulfilled
               $changes++;
            }
         } ## end for my $needed (keys $slot...)
         die "no valid candidate for $i" unless scalar keys $slot->%*;
      } ## end else [ if ($is_exact{$i}) ]
   } ## end for my $i (0 .. $#$solution)
   return $changes;
} ## end sub constraint_basic ($status)

sub constraint_weighted_sum ($status) {
   my $solution = $status->{solution};
   my $n = my $available_slots = $solution->@*;
   my %chip_for;
   for my $i (0 .. $n - 1) {
      my $slot = $solution->[$i];
      my $chip = $chip_for{$i} = min(keys $slot->%*) * $i;
      $available_slots -= $chip;
   }
   my $deleted = 0;
   for my $i (1 .. $n - 1) {
      my $slot = $solution->[$i];
      my $max  = int(($available_slots + $chip_for{$i}) / $i);
      for my $j ($max + 1 .. $n - 1) {
         $deleted++ if delete $slot->{$j};
      }
   } ## end for my $i (1 .. $n - 1)
   return $deleted;
} ## end sub constraint_weighted_sum ($status)
