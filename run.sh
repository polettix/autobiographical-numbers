#!/bin/sh
md="$(dirname "$(readlink -f "$0")")"
stage="${1:-"01-basic"}"
N="${2:-"4"}"
perl "$md/$stage/autobiographical-numbers.pl" "$N"
